__author__ = 'jb'

import matplotlib
matplotlib.use('Qt4Agg')
import sys
import numpy as np
import math
import matplotlib.pyplot as plt

ARRAY_SHAPE = (1000, 1000)
ARRAY_SCALE = (100.0, 100.0)
array = np.zeros(ARRAY_SHAPE)

SOURCE_POSITIONS = (
    (0, 0),
    (100, 0),
    (200, 0),
    (300, 0),
    (400, 0),
    (500, 0),
    (600, 0),
    (700, 0),
    (800, 0),
    (900, 0),
    (1000, 0),
)

for ii in range(ARRAY_SHAPE[0]):
    for jj in range(ARRAY_SHAPE[1]):
        for ii_diff, jj_diff in SOURCE_POSITIONS:
            # array[ii, jj] = math.sin(math.pow((ii*ii+ jj*jj) / ARRAY_SCALE[0], 0.5))
            ii_moved = ii - ii_diff
            jj_moved = jj - jj_diff
            array[ii, jj] += math.sin(math.pow((ii_moved*ii_moved/ARRAY_SCALE[0]+ jj_moved*jj_moved/ARRAY_SCALE[1]), 0.5))

array*=array

plt.ion()

plt.imshow(array)
plt.colorbar()

plt.savefig('3d.png')
plt.clf()
plt.plot(array[500, :])
plt.savefig('2dx.png')
plt.clf()
plt.plot(array[:, 500])
plt.savefig('2dy.png')

#
# for line in sys.stdin:
#     print line