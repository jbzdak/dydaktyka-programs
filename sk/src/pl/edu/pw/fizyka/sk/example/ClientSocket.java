package pl.edu.pw.fizyka.sk.example;

import java.io.*;
import java.net.Socket;

public class ClientSocket {
    public static void main(String[] args) throws Exception{
        Socket socket = new Socket("lfitj.if.pw.edu.pl", 80);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bufferedWriter.write("GET /dydaktyka/sk/zaj2.html HTTP/1.0\n\n");
        bufferedWriter.flush();
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line = reader.readLine();
        while (line!=null){
            System.out.println(line);
            System.out.flush();
            line = reader.readLine();
        }
    }
}
