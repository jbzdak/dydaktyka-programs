package pl.edu.pw.fizyka.sk.tictactoe.solution;

import pl.edu.pw.fizyka.sk.tictactoe.solution.TextHumanPlayer;

import java.io.*;
import java.net.Socket;

public class SocketPlayer extends TextHumanPlayer {

    final Socket socket;

    final PrintStream printStream;

    final BufferedReader reader;

    public SocketPlayer(Socket socket) throws IOException {
        this.socket = socket;
        this.printStream = new PrintStream(socket.getOutputStream());
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public PrintStream getOutputStream() {
        return printStream;
    }

    @Override
    public String getInputLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
