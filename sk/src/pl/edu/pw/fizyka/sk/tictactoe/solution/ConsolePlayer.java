package pl.edu.pw.fizyka.sk.tictactoe.solution;

import java.io.PrintStream;

public class ConsolePlayer extends TextHumanPlayer {

    @Override
    public PrintStream getOutputStream() {
        return System.out;
    }

    @Override
    public String getInputLine() {
        return System.console().readLine();
    }
}
