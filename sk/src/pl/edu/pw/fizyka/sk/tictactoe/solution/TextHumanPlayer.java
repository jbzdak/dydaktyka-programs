package pl.edu.pw.fizyka.sk.tictactoe.solution;

import pl.edu.pw.fizyka.sk.tictactoe.api.AbstractPlayer;
import pl.edu.pw.fizyka.sk.tictactoe.api.Board;
import pl.edu.pw.fizyka.sk.tictactoe.api.State;

import java.io.PrintStream;
import java.util.List;

public abstract class TextHumanPlayer extends AbstractPlayer {

    public static final String GET_LEGAL_MOVE = "Please provide a legal move?";
    public static final String YOUR_MOVE = "Your move?";

    public abstract PrintStream getOutputStream();
    
    public abstract String getInputLine();

    private void printBasicInfo(State[] board){
        getOutputStream().println("Symbol: " + symbol);
        getOutputStream().println("Board:");
        Board.print(getOutputStream(), board);
    }

    @Override
    public void notifyGameEnd(State[] board, State winner) {
        printBasicInfo(board);
        getOutputStream().println("Winner is: " + winner);
    }

    @Override
    public int nextMove(State[] board) {
        printBasicInfo(board);
        getOutputStream().println("Legal moves:");
        List<Integer> legalMoves = Board.emptyPlaces(board);
        getOutputStream().print(legalMoves);
        getOutputStream().println(YOUR_MOVE);
        while (true){
            String s = getInputLine().trim();
            int move;
            try{
                move = Integer.parseInt(s);
            }catch (NumberFormatException e){
                getOutputStream().print("Wrong format");
                continue;
            }
            if (!legalMoves.contains(move)){
                getOutputStream().print(GET_LEGAL_MOVE);
                continue;
            }
            return move;
        }
    }
}
