package pl.edu.pw.fizyka.sk.tictactoe.api;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class AiPlayer extends AbstractPlayer {

    private final static int[] CORNERS = {4, 0, 2, 6 , 8};

    private Random random = new Random();



    @Override
    /**
     * Simple AI for tic tac toe game. It has following algorithm:
     * <ul>
     *     <li><p> If it has winning move it makes it
     *     <li><p> If oponnent has winning move it blocks it
     *     <li><p> If this is first move takes position 5
     *     <li><p> Takes position in random corner if avilable
     *     <li><p> Takes random position
     * </ul>
     */
    public int nextMove(State[] board) {

        List<Integer> empty = Board.emptyPlaces(board);

        //Take middle if start
        if (empty.size() == 9){
            return 4;
        }

        // Make winning move
        for (Integer place : empty){
            State[] copy = Arrays.copyOf(board, board.length);
            copy[place] = symbol;
            if(Board.winner(copy, symbol)){
                return place;
            }
        }

        // Block opponent wining move
        for (Integer place : empty){
            State[] copy = Arrays.copyOf(board, board.length);
            copy[place] = enemy;
            if(Board.winner(copy, enemy)){
                return place;
            }
        }

        //Take corner or middle
        for (int corner : CORNERS){
            if (empty.contains(corner)){
                return corner;
            }
        }
        //Make random move
        return empty.get(random.nextInt(empty.size()));
    }
}
