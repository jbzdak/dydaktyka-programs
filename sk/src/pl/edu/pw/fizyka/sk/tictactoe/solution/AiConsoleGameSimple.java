package pl.edu.pw.fizyka.sk.tictactoe.solution;

import pl.edu.pw.fizyka.sk.tictactoe.api.AiPlayer;
import pl.edu.pw.fizyka.sk.tictactoe.api.Board;
import pl.edu.pw.fizyka.sk.tictactoe.api.Player;
import pl.edu.pw.fizyka.sk.tictactoe.api.State;

import java.io.*;

public class AiConsoleGameSimple {

    public static void main(String[] args) throws IOException {
        Player aiPlayer = new AiPlayer();
        aiPlayer.setSymbol(State.X);

        Board board = new Board();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in)); //Opakowanie standardowego wejścia
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out)); //Opakowanie standardowego wyjścia

        while (board.getLegalMoves().size()>0){

            bufferedWriter.write("Plansza:\n");
            Board.print(bufferedWriter, board.getBoard());
            bufferedWriter.write("Twój ruch:\n");
            bufferedWriter.flush();

            String line = bufferedReader.readLine();

            board.play(Integer.parseInt(line), State.O);

            if (board.winner() != null){
                bufferedWriter.write("Wygrał: " + board.winner());
                bufferedWriter.write("Plansza:\n");
                Board.print(bufferedWriter, board.getBoard());
                return;
            }

            board.play(aiPlayer.nextMove(board.getBoard()), State.X);

            if (board.winner() != null){
                bufferedWriter.write("Wygrał: " + board.winner());
                bufferedWriter.write("Plansza:\n");
                Board.print(bufferedWriter, board.getBoard());
                return;
            }

        }

    }
}
