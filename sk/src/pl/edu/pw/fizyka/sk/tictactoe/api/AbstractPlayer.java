package pl.edu.pw.fizyka.sk.tictactoe.api;

public abstract class AbstractPlayer implements Player {
    protected State symbol;
    protected State enemy;

    @Override
    public void notifyGameEnd(State[] board, State winner) {

    }

    @Override
    public void setSymbol(State symbol) {
        this.symbol = symbol;
        if (symbol == State.X){
            this.enemy = State.O;
        }else {
            this.enemy = State.X;
        }

    }
}
