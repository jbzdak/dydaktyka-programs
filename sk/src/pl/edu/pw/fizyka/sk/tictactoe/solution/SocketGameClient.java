package pl.edu.pw.fizyka.sk.tictactoe.solution;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketGameClient {

    public static void main(String[] args) throws IOException {
        Socket s = new Socket("localhost", 12346);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
//        PrintStream stream = new PrintStream(s.getOutputStream());
        while (true){
            String line = bufferedReader.readLine();
            System.out.println(line);
            if (line.contains("Winner")){
                return;
            }
            if (line.contains(TextHumanPlayer.YOUR_MOVE)){
                System.out.println("Read");
                String LINE = System.console().readLine();
                System.out.println("READ " + LINE);
                s.getOutputStream().write(LINE.getBytes());
                s.getOutputStream().write("\n".getBytes());
                s.getOutputStream().flush();
            }

        }

    }
}
