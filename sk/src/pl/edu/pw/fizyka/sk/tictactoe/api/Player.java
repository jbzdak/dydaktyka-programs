package pl.edu.pw.fizyka.sk.tictactoe.api;

public interface Player {

    /**
     * Informuje gracza o zakończeniu rozgrywki
     * @param board Końcowy stan planszy
     * @param winner Zwycięzca
     */
    void notifyGameEnd(State[] board, State winner);

    /**
     * Zwraca następny ruch gracza
     * @param board plansza
     * @return następny ruch gracza
     */
    int nextMove(State[] board);

    /**
     * Ustawia symbol jakim gra dany gracz
     * @param symbol
     */
    void setSymbol(State symbol);
}
