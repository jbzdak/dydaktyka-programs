package pl.edu.pw.fizyka.sk.tictactoe.api;

public enum State {
    X, O, DRAW;

    public static String label(State state, int index){
        if(state == null){
            return String.valueOf(index);
        }
        return state.name();
    }
}
