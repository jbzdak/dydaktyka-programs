package pl.edu.pw.fizyka.sk.tictactoe.api;

public class Game {

    final Player playerA;
    final Player playerB;
    final Board board = new Board();

    public Game(Player playerA, Player playerB) {
        super();
        this.playerA = playerA;
        this.playerB = playerB;
    }


    /**
     * Performs game and returns the winner
     * @return
     */
    public State performGame(){
        State winner = performGamePrivate();
        playerA.notifyGameEnd(board.getBoard(), winner);
        playerB.notifyGameEnd(board.getBoard(), winner);
        return winner;
    }

    private State performGamePrivate(){
        playerA.setSymbol(State.O);
        playerB.setSymbol(State.X);
        while (board.getLegalMoves().size()>0){
            board.play(playerA.nextMove(board.getBoard()), State.O);
            if (board.winner() != null){
                return board.winner();
            }
            board.play(playerB.nextMove(board.getBoard()), State.X);
            if (board.winner() != null){
                return board.winner();
            }
        }
        return null;
    }
}
