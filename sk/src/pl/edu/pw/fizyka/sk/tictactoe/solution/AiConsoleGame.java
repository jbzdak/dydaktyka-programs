package pl.edu.pw.fizyka.sk.tictactoe.solution;

import pl.edu.pw.fizyka.sk.tictactoe.api.AiPlayer;
import pl.edu.pw.fizyka.sk.tictactoe.api.Game;

public class AiConsoleGame {

    public static void main(String[] args){
        Game game = new Game(new ConsolePlayer(), new AiPlayer());
        game.performGame();
    }
}
