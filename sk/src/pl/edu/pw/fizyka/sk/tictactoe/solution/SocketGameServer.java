package pl.edu.pw.fizyka.sk.tictactoe.solution;

import pl.edu.pw.fizyka.sk.tictactoe.api.AiPlayer;
import pl.edu.pw.fizyka.sk.tictactoe.api.Game;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketGameServer  {

    private static int PORT = 12346;

    String host = "localhost";

    public static void main(String[] args) throws Exception{
        ExecutorService executor = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(PORT);

        while (true){
            final Socket socket = serverSocket.accept();
            System.out.printf("Accepting");
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    Game game = null;
                    try {
                        game = new Game(new SocketPlayer(socket), new AiPlayer());
                    } catch (IOException e) {
                        throw new RuntimeException();
                    }
                    game.performGame();
                }
            });

        }

    }
}
