package pl.edu.pw.fizyka.sk.tictactoe.api;



import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board {

    private final State[] board = new State[9];

    private static final int[][] WINNING_STATES = {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8},
            {0, 3, 6},
            {1, 4, 7},
            {2, 5, 8},
            {0, 4, 8},
            {2, 4, 6}
    };

    public State[] getBoard(){
        return Arrays.copyOf(board, board.length);
    }

    /**
     * Powoduje umieszczenie symbolu gracza na polu o ineksie
     * @param index indeks w którym stawiamu znak (z zakresu 0 -- 9)
     * @param player symbol gracza
     */
    public void play(int index, State player){
        if (!legalMove(board, index)){
            throw new IllegalStateException("Place where you put your mark must be empty");
        }
        board[index] = player;
    }

    /**
     * Lista poprawnych ruchów
     * @return
     */
    public List<Integer> getLegalMoves(){
        return emptyPlaces(board);
    }

    /**
     * Zwraca zwycięzcę, albo null jeśli nie ma zwycięzcy
     * @return
     */
    public State winner(){
        if (winner(board, State.X)){
            return State.X;
        }
        if (winner(board, State.O)){
            return State.O;
        }
        if (getLegalMoves().size()==0){
            return State.DRAW;
        }
        return null;
    }

    /**
     * Wyświetla planszę na OutputStream
     * @param stream strumień do którego zapiszemy planszę
     * @param board plansza do zapisania
     */
    public static void print(OutputStream stream, State[] board){
        if (board.length !=9){
            throw new IllegalStateException("Board.length must be equal to 9.");
        }

        PrintStream printStream;
        if (stream instanceof  PrintStream){
            printStream = (PrintStream) stream;
        }else {
            printStream = new PrintStream(stream);
        }

        for (int ii = 0;  ii < 3; ii++){
            for(int jj = 0; jj < 3; jj++){
                int index = ii * 3 + jj;
                printStream.print(State.label(board[index], index));
                if (jj <2){
                    printStream.print("|");
                }
            }
            printStream.print("\n");
        }
    }

    public static void print(Writer writer, State[] board) throws IOException{
        if (board.length !=9){
            throw new IllegalStateException("Board.length must be equal to 9.");
        }

        BufferedWriter bufferedWriter;
        if (writer instanceof BufferedWriter){
            bufferedWriter = (BufferedWriter) writer;
        }else{
            bufferedWriter = new BufferedWriter(writer);
        }

        for (int ii = 0;  ii < 3; ii++){
            for(int jj = 0; jj < 3; jj++){
                int index = ii * 3 + jj;
                bufferedWriter.write(State.label(board[index], index));
                if (jj <2){
                    bufferedWriter.write("|");
                }
            }
            bufferedWriter.write("\n");
        }

        bufferedWriter.flush();
    }

    public static boolean legalMove(State[] board, int index){
        return board[index] == null;
    }

    public static List<Integer> emptyPlaces(State[] board){
        List<Integer> result = new ArrayList<Integer>(9);
        for (int ii = 0; ii < board.length; ii++){
            if (board[ii] == null){
                result.add(ii);
            }
        }
        return result;
    }

    public static boolean winner(State[] board, State player){
        /**
         * Checks if player won the game
         */
        if (board.length !=9){
            throw new IllegalStateException("Board.length must be equal to 9.");
        }

        for (int ii=0; ii < WINNING_STATES.length; ii++){
            boolean won = true;
            for (int index : WINNING_STATES[ii]){
                if (board[index] != player){
                    won = false;
                    break;
                }
            }
            if(won)
                return true;
        }
        return false;
    }



}
